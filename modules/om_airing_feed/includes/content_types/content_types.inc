<?php

/**
* function to create CCK content types
*/
function _om_airing_feed_install_create_content_types() {

  // in order to create CCK content types, we use the form-based content_copy import/export functionality
  
  // create om_feed content type
  module_load_include('inc', 'om_airing_feed', 'includes/content_types/om_airing_feed_content_type');
  _om_airing_feed_install_create_om_feed_content_type();

  return;
} // function _om_airing_feed_install_create_content_types


/**
* function to import a new content type using CCK import functionality
*/
function _om_airing_feed_install_import_content_type($macro) {
  $form_state = array();
  $form = content_copy_import_form($form_state);
  $form_state['values']['type_name'] = '<create>';
  $form_state['values']['macro'] = $macro;
  return content_copy_import_form_submit($form, $form_state);
} // function _om_airing_feed_install_import_content_type  

