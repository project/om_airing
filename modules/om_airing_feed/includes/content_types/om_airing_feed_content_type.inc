<?php

/**
* function to create Open Media Broadcast Server Schedule Feed CCK content type
*/
function _om_airing_feed_install_create_om_feed_content_type() {

  $result = _om_airing_feed_install_import_content_type ("\$content[type]  = array (
  'name' => 'Airing Feed',
  'type' => 'om_airing_feed',
  'description' => '<b>Open Media System</b> - The content type is used to add RSS feeds from playback servers that keep the Drupal version of the schedule synchronized with the servers.',
  'title_label' => 'Title',
  'body_label' => '',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array (
    'status' => true,
    'promote' => true,
    'sticky' => false,
    'revision' => false,
  ),
  'scheduler' => 0,
  'scheduler_touch' => 0,
  'old_type' => 'om_feed',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'location_maxnum' => '0',
  'location_defaultnum' => '0',
  'location_addanother' => 0,
  'location_weight' => '9',
  'location_collapsible' => 1,
  'location_collapsed' => 1,
  'location_rss' => 'simple',
  'og_content_type_usage' => 'omitted',
  'comment' => '0',
  'comment_default_mode' => '4',
  'comment_default_order' => '1',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => 0,
  'comment_subject_field' => '1',
  'comment_preview' => '1',
  'comment_form_location' => '0',
  'feedapi' => 
  array (
    'enabled' => 1,
    'refresh_on_create' => 0,
    'update_existing' => 1,
    'skip' => 0,
    'items_delete' => '0',
    'parsers' => 
    array (
      'parser_simplepie' => 
      array (
        'enabled' => 1,
        'weight' => '0',
      ),
    ),
    'processors' => 
    array (
      'feedapi_node' => 
      array (
        'enabled' => 1,
        'weight' => '1',
        'content_type' => 'om_airing',
        'node_date' => 'feed',
        'promote' => '3',
        'x_dedupe' => '0',
      ),
      'feedapi_inherit' => 
      array (
        'enabled' => 1,
        'weight' => '2',
        'inherit_og' => 0,
        'inherit_taxonomy' => 1,
      ),
    ),
  ),
);");

  return;
} // function _om_airing_feed_install_create_om_feed_content_type 
